package org.stepanova.tests;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.stepanova.tests.BaseSeleniumTestClass;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AlertTest extends BaseSeleniumTestClass {

    @BeforeClass
    public void BeforeClass() {
        goToUrl("http://the-internet.herokuapp.com/javascript_alerts");
    }

    @Test
    public void AlertConfirmOkTest() {
        clickOnButton(AlertButtons.CONFIRM);
        switchToAlert(true);
        Assert.assertEquals(getResultText(), "You clicked: Ok");
    }

    @Test
    public void AlertConfirmCancelTest() {
        clickOnButton(AlertButtons.CONFIRM);
        switchToAlert(false);
        Assert.assertEquals(getResultText(), "You clicked: Cancel");
    }

    @Test
    public void PromptOkTextTest() {
        String text = "Test";
        clickOnButton(AlertButtons.PROMPT);
        sendText(text);
        switchToAlert(true);
        Assert.assertEquals(getResultText(), "You entered: " + text);
    }

    @Test
    public void PromptOkWithoutTextTest() {
        clickOnButton(AlertButtons.PROMPT);
        switchToAlert(true);
        Assert.assertEquals(getResultText(), "You entered:");
    }

    @Test
    public void PromptCancelTextTest() {
        String text = "Test";
        clickOnButton(AlertButtons.PROMPT);
        sendText(text);
        switchToAlert(false);
        Assert.assertEquals(getResultText(), "You entered: null");
    }

    @Test
    public void PromptCancelWithoutTextTest() {
        clickOnButton(AlertButtons.PROMPT);
        switchToAlert(false);
        Assert.assertEquals(getResultText(), "You entered: null");
    }


    public void clickOnButton(AlertButtons button) {
        driver
                .findElement(By.xpath("//button[contains(text(),'" + button.getTextOnButton() + "')]"))
                .click();
    }


    enum AlertButtons {
        ALERT("Click for JS Alert"),
        CONFIRM("Click for JS Confirm"),
        PROMPT("Click for JS Prompt");

        private String textOnButton;

        AlertButtons(String textOnButton) {
            this.textOnButton = textOnButton;
        }

        public String getTextOnButton() {
            return textOnButton;
        }
    }

    public void switchToAlert(boolean confirm) {
        Alert alert = driver.switchTo().alert();
        if (confirm) {
            alert.accept();
        } else {
            alert.dismiss();
        }
    }

    public String getResultText() {
        return driver.findElement(By.cssSelector("#result")).getText();
    }

    public void sendText(String text) {
        Alert alert = driver.switchTo().alert();
        alert.sendKeys(text);
    }

}
