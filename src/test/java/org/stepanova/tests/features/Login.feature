Feature: Login

Scenario: Check login with valid data
  Given User is on login page
  And User enters "dstepanova@gmail.com" in the email field
  And User enters "12345678" in the password field
  And User clicks login button
  Then User should see my account button
  And User should see log out button
