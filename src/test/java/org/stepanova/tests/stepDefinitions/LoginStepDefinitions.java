package org.stepanova.tests.stepDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginStepDefinitions {

    public WebDriver driver;


    @Given("User is on login page")
    public void user_is_on_login_page() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.navigate().to("http://54.37.125.177/login");
    }

    @When("User enters {string} in the email field")
    public void user_enters_in_the_email_field(String string) {
        driver.findElement(By.className("email")).sendKeys(string);
    }

    @When("User enters {string} in the password field")
    public void user_enters_in_the_password_field(String string) {
        driver.findElement(By.className("password")).sendKeys(string);
    }

    @When("User clicks login button")
    public void user_clicks_login_button() {
        driver.findElement(By.cssSelector(".button-1.login-button")).click();
    }

    @Then("User should see log out button")
    public void user_should_see_log_out_button() {
        driver.findElement(By.cssSelector(".ico-account")).isDisplayed();
    }

    @Then("User should see my account button")
    public void user_should_see_my_account_button() {
        driver.findElement(By.cssSelector(".ico-logout")).isDisplayed();
    }

    @After
    public void finishTest(){
        driver.quit();
    }



}
