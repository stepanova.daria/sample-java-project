package org.stepanova.driver;

public enum Browsers {
    CHROME,
    FIREFOX,
    EDGE
}
